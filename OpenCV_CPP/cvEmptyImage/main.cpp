#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;
using namespace std;

int main(int, char**) {
    // create a new image which consists of 3 channels
	// image depth of 8 bits
	// 800 x 600 of resolution
	// each pixel initialized to the value of (100, 250, 30) for blue, green and red
	Mat image(600, 800, CV_8UC3, Scalar(100, 250, 30));

	String windowName = "Window with Blank Image";

	namedWindow(windowName);
	imshow(windowName, image);

	waitKey(0);

	destroyWindow(windowName);

	return 0;
}
